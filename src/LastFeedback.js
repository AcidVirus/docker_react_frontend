import React, {Component} from 'react';
import {render} from "react-dom";
import './LastFeedback.css';

class LastFeedback extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            text: null
        };
    }

    render() {
        let email = this.props.email
        let text = this.props.text
        return (
            <div className="last-feedback">
                <h2>
                    Самый свежий отзыв
                </h2>
                <div>
                    <div>
                        {text}
                    </div>
                    <div className="text-right">
                        <i>
                            <b>
                                {email}
                            </b>
                        </i>
                    </div>
                </div>
            </div>
        );
    }
}

export default LastFeedback;

const container = document.getElementById("root");
render(<LastFeedback/>, container);