import React, {Component} from 'react';
import {render} from "react-dom";
import './SendFeedback.css';
import API from "./API";
import LastFeedback from "./LastFeedback";

class Feedback extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            email: null,
            text: null
        };
    }

    onSubmit = (e) => {
        e.preventDefault();
        let email = e.target.elements[0].value;
        let text = e.target.elements[1].value;

        API.post('feedback/', {email, text})
            .then(response => {
                this.setState(() => {
                    return {
                        email: response.data.email,
                        text: response.data.text,
                    };
                });
            });
    }

    componentDidMount() {
        this._isMounted = true;

        API.get('feedback/')
            .then(response => {
                if (this._isMounted) {
                    this.setState(() => {
                        return {
                            email: response.data.email,
                            text: response.data.text,
                        };
                    });
                }
            }).catch(() => {
            this.setState(() => {
                return {
                    email: '(с) Лао Цзы',
                    text: 'Кто оставит отзыв, тот оставит отзыв!',
                };
            });
        });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        return (
            <div>
                <LastFeedback email={this.state.email} text={this.state.text}/>
                <div className="send-feedback">
                    <form onSubmit={this.onSubmit}>
                        <div className="form-field">
                            <label htmlFor="email">E-mail</label>
                            <div>
                                <input id="email" name="email" type="text"/>
                            </div>
                        </div>
                        <div className="form-field">
                            <label htmlFor="text">Отзыв</label>
                            <div>
                                <textarea name="text" id="text" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div className="form-field">
                            <button type="submit">Отпраить</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Feedback;

const container = document.getElementById("root");
render(<Feedback/>, container);